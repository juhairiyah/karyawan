const karyawan = [
    {id:'1', nama:'Andrean Prenata', jabatan:'developer master',
    email:'Andrean@gmail.com', phone:'087888666555', gaji:'7.000.000',
    foto:'https://bit.ly/3OQRiBt'},

    {id:'2', nama:'Ananda Pranata', jabatan:'Desaigner',
    email:'Anandapra@gmail.com', phone:'087888768540', 
    gaji:'6.500.000',
    foto:'https://bit.ly/3uzngtL'},

    {id:'3', nama:'Amanda Manopo', jabatan:' Web developer master',
    email:'Manoopo@gmail.com', phone:'081856796595', gaji:'8.000.000',
    foto:'https://bit.ly/3ypUJYL'},

    {id:'4', nama:'Sabil Wilyansyah', jabatan:'Backend developer',
    email:'sabiwil@gmail.com', phone:'087808964558', gaji:'8.400.000',
    foto:'https://bit.ly/3Pf464v'},

    {id:'5', nama:'Marquee Jhon', jabatan:'UI Desaigner',
    email:'marqjhon@gmail.com', phone:'087878064505', gaji:'5.700.000',
    foto:'https://bit.ly/3PdnnmC'},
];
export default karyawan;
