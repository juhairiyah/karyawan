import React from "react";
import {Text, View, StyleSheet, Button} from 'react-native';

const Tentang = ({route,navigation}) => {
    //mengambil data dari halaman sebelumnya
    const {namaDepan} = route.params;
    const {namaBelakang} = route.params;

    return(
        <View style={styles.pertama}>
            <Text>Halaman Tentang</Text>
            {/* tampilkan data */}
            <Text>Nama Depan : {namaDepan}</Text>
            <Text>Nama Belakang : {namaBelakang}</Text>
            <Button title="kembali"
            onPress={()=>navigation.navigate('Home')}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    pertama: {
        alignItems:'center'
    }
})

export default Tentang;